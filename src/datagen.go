package main

import (
  "fmt"
  "flag"
  "math/rand"
  "time"
)

func main() {
  len := flag.Int("len", 1000000, "Number of lines to generate")
  min := flag.Int("min", 0, "Minimum Integer")
  max := flag.Int("max", 1000000000, "Maximum Integer")
  flag.Parse()
  sumInts := 0
  for sumInts < *len {
    rand.Seed(time.Now().UnixNano())
    fmt.Println(rand.Intn(*max - *min) + *min)
    sumInts += 1
  }
}
