package main

import (
  "fmt"
  "flag"
  "bufio"
  "strconv"
  "os"
  "log"
  "sort"
  "github.com/wangjohn/quickselect"
)

func main() {
  n := flag.Int("n", 10, "n number of top results")
  flag.Parse()

  integers, err := ReadIntFile("dataset.txt")

  if err != nil {
    log.Fatalf("readLines: %s", err)
  }

  quickselect.QuickSelect(quickselect.Reverse(quickselect.IntSlice(integers)), *n)
  sort.Sort(sort.Reverse(sort.IntSlice(integers[:*n])))

  for _, i := range integers[:*n] {
    fmt.Println(i)
  }
}


func ReadIntFile(path string) ([]int, error) {
  file, err := os.Open(path)
  if err != nil {
    return nil, err
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  scanner.Split(bufio.ScanWords)
  integers := make([]int, 0)
  for scanner.Scan() {
    x, err := strconv.Atoi(scanner.Text())
    if err != nil {
      return integers, err
    }
    integers = append(integers, x)
  }
  return integers, scanner.Err()
}
