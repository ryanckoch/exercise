# Exercise

## Generate some bunk data
```
./datagen -h
./datagen >> dataset.txt

```
## Run topN
```
./topN -h
./topN
```

## My results (with larger dataset.txt)
```
# I eventually kill this for time ending at 1159405054 integers
rkoch@rkoch:~/exercise$ ./datagen -len 2000000000 -max 100000000000000000 >> dataset.txt

rkoch@rkoch:~/exercise$ wc -l dataset.txt
1159405054 dataset.txt

rkoch@rkoch:~/exercise$ wc -l dataset.txt
1159405054 dataset.txt

rkoch@rkoch:~/exercise$ ls -lh dataset.txt
-rw-rw-r-- 1 rkoch rkoch 14G Mar  1 20:16 dataset.txt

rkoch@rkoch:~/exercise$ time ./topN
99999999759536953
99999999759536953
99999999759536953
99999999617023278
99999999617023278
99999999449018061
99999999356918383
99999999352283444
99999999352283444
99999998749096457

real	11m27.431s
user	10m21.584s
sys	1m21.176s
```
